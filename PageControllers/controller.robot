*** Settings ***
Library       SeleniumLibrary
Resource      ../PageObjects/elements.robot
Resource      ../Utils/actions.robot


*** Variable ***
@{FRUTAS}     maça                                                                                                 banana    uva    abacaxi
${options}    add_argument("--disable-dev-shm-usage"); add_argument("--headless"); add_argument("--no-sandbox")

*** Keywords ***
acessar site

    Open Browser                        ${SITE}                                                                                           ${BROWSER}    options=${options}
    Maximize Browser Window
    Wait Until Element Is Visible       ${HOME_LOGO}                                                                                      10

finalizar navegador
    Close Browser

reiniciar site
    click                               ${HOME_ICON}
    Wait Until Element Is Visible       ${HOME_LOGO}                                                                                      10

pesquisar produto "${PRODUTO}"
    input                               ${INPUT_PRODUTO}                                                                                  ${PRODUTO}
    click                               ${BUTTON_PESQUISAR}
    Wait Until Element Is Visible       ${HOME_ICON}

conferir produto "${PRODUTO}"
    Title Should Be                     Search - My Store
    Page Should Contain Image           xpath=//img[@src='http://automationpractice.com/img/p/7/7-home_default.jpg']
    Page Should Contain Element         xpath=//div[@id='center_column']//span[contains(text(), '${PRODUTO}')]

conferir produto invalido "${PRODUTO}"
    Title Should Be                     Search - My Store
    Page Should Contain Element         xpath=//div[@id='center_column']//p[contains(text(), 'No results were found for your search')]
    Page Should Contain Element         xpath=//p[contains(text(), '${PRODUTO}')]

selecionar categoria "${SUB_CATEGORIA}"
    Mouse Over                          ${CATEGORIA}
    Wait Until Element Is Visible       xpath=//a[contains(text(), '${SUB_CATEGORIA}')]
    click                               xpath=//a[contains(text(), '${SUB_CATEGORIA}')]

validar resultado da lista de produtos "${SUB_CATEGORIA}"
    Page Should Contain Element         xpath=//span[@class='category-name'][contains(text(), '${SUB_CATEGORIA}')]
    Wait Until Page Contains Element    ${PRODUTO}
    Page Should Contain Element         ${PRODUTO}

adicionar produto no carrinho
    Mouse Over                          ${PRODUTO}
    click                               ${ADD_CART}
    Wait Until Element Is Visible       ${PRODUTO_ADICIONADO_COM_SUCESSO}                                                                 10
    Page Should Contain Element         ${PRODUTO_ADICIONADO_COM_SUCESSO}

visualizar carrinho
    click                               ${CHECKOUT}
    Wait Until Element Is Visible       ${CARRINHO} 20
    Page Should Contain Element         ${CARRINHO}

for com range
    :FOR  ${index}  IN RANGE  1  6
    \    Log                            contador atual ${index}/5
    \    Run Keyword If                 ${index} == 5                                                                                     Log           acabouj o loop
    Log                                 Testando

for com list
    :FOR  ${item}  IN  @{FRUTAS}
    \    Log                            Minha fruta é: ${item}
    \    Run Keyword If                 '${item}' == 'abacaxi'                                                                            Log           acabou o loop!!

saindo do for
    :FOR  ${item}  IN  @{FRUTAS}
    \    Exit For Loop If               '${item}' == 'uva'
    \    Log                            Minha fruta é: ${item}

usando repeat
    Repeat Keyword                      4                                                                                                 Log           Vamos logar 4 vezes esta mensagem.


