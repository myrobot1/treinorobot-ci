*** Settings ***
Library                              SeleniumLibrary

*** Variable ***
${SITE}                              http://automationpractice.com/index.php
${BROWSER}                           headlesschrome
${HOME_LOGO}                         xpath=//img[@src='http://automationpractice.com/img/logo.jpg']
${INPUT_PRODUTO}                     id=search_query_top
${BUTTON_PESQUISAR}                  name=submit_search
${HOME_ICON}                         xpath=//i[contains(@class,'icon-home')]
${CATEGORIA}                         xpath=//ul/li/a[contains(text(),'Women')]
${PRODUTO}                           xpath=//div[contains(@class, 'product-container')]
${ADD_CART}                          xpath=//span[text()='Add to cart']
${PRODUTO_ADICIONADO_COM_SUCESSO}    xpath=//h2[contains(., 'Product successfully added to your shopping cart')]
${CHECKOUT}                          xpath=//span[contains(., 'Proceed to checkout')]
${CARRINHO}                          xpath=//h1[contains(text(), 'Shopping-cart summary')]
