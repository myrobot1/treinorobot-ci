*** Settings ***
Resource                  ../PageControllers/controller.robot
Resource                  ../PageObjects/elements.robot

Suite Setup                  acessar site
Test Teardown                reiniciar site
Suite Teardown               finalizar navegador

*** Variable ***
&{PESSOA}       nome=Bruno Moraes  email=moraes.7bruno@gmail.com  idade=20  sexo=Masculino

*** Test Case ***
Teste 01: pesquisar produto valido
    pesquisar produto "blouse"
    conferir produto "blouse"

Teste 02: pesquisar produto invalido
    pesquisar produto "renner"
    conferir produto invalido "renner"

Teste 03: lista de produtos
    selecionar categoria "Summer Dresses"
    validar resultado da lista de produtos "Summer Dresses"

# Teste 04: adicionar produto ao carrinho
#     pesquisar produto "blouse"
#     adicionar produto no carrinho
#     visualizar carrinho
    
