*** Settings ***
Library    Selenium2Library

*** Keywords ***
input
    [Arguments]                         ${element}              ${message}      ${press_keys}=False    ${timeot}=10
    Wait Until Page Contains Element    ${element}              ${timeot}
    Wait Until Element Is Visible       ${element}              ${timeot}
    Run Keyword If                      ${press_keys}           Press Keys      ${element}             ${message}
    Run Keyword If                      ${press_keys}==False    Input Text      ${element}             ${message}

click
    [Arguments]                         ${element}              ${timeot}=10
    Wait Until Page Contains Element    ${element}              ${timeot}
    Wait Until Element Is Visible       ${element}              ${timeot}
    Click Element                       ${element}
